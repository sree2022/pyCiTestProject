from re import search
import requests
import pytest

search = "search"
contentType = "application/json"

@pytest.fixture
def api_Key():
    return 'XgaMjEV_xVeo-W8AszY0OQc0AeVQOIxM5By4TptDhnCp8n3kEPRmQryeQC3KwN-0yW1feTEvLzHaRgHmGk38X66xP0QnG3O55VJrsw0Ec5KMtLiI-lljfXP6Zh8uYnYx'

@pytest.fixture
def params():
    return {'term': 'coffee', 
            'location': 'Toronto, Ontario',
            'limit': 50}

@pytest.fixture
def headers(api_Key):
    return {'Authorization': 'Bearer {}'.format(api_Key)}

@pytest.fixture
def api_url():
    return 'https://api.yelp.com/v3/businesses/'

def test_verifyStatusCode(api_url, headers, params):
    response = requests.get(api_url+search, headers=headers,params=params, timeout=5)
    assert response.status_code==200


def test_verifyContentType(api_url, headers, params):
    response = requests.get(api_url+search, headers=headers,params=params, timeout=5)
    assert response.headers["Content-Type"] == contentType


def test_verifyBusinessById(api_url, headers):
    businessId = 'qXFjkI_L3Ws3diveYocNFg'
    data_dict = requests.get(api_url+businessId, headers=headers, timeout=5).json()
    expected_Business = "Coffee Lunar"
    assert data_dict["name"] == expected_Business


